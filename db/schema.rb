# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_07_30_215120) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "pg_trgm"
  enable_extension "plpgsql"
  enable_extension "postgis"

  create_table "books", force: :cascade do |t|
    t.string "title", null: false
    t.integer "publisher_id", null: false
  end

  create_table "publishers", force: :cascade do |t|
    t.string "title", null: false
  end

  create_table "shop_books", force: :cascade do |t|
    t.integer "count", default: 1, null: false
    t.integer "shop_id"
    t.integer "book_id"
    t.integer "sold_count", default: 0, null: false
  end

  create_table "shops", force: :cascade do |t|
    t.string "name", null: false
  end

  create_table "spatial_ref_sys", primary_key: "srid", id: :integer, default: nil, force: :cascade do |t|
    t.string "auth_name", limit: 256
    t.integer "auth_srid"
    t.string "srtext", limit: 2048
    t.string "proj4text", limit: 2048
    t.check_constraint "(srid > 0) AND (srid <= 998999)", name: "spatial_ref_sys_srid_check"
  end

  add_foreign_key "books", "publishers"
  add_foreign_key "shop_books", "books"
  add_foreign_key "shop_books", "shops"
end
